// Crates
use bevy::prelude::*;


// Bevy Plugin
pub struct TileMapPlugin {}

impl Plugin for TileMapPlugin {
    fn build(&self, app: &mut App) {}
}

pub struct Tile {
    pub tile_id: u8
}

pub struct Chunk {
    tiles: [[Tile; 16]; 16]
}

pub struct TileMapLayer {
    chunks: Vec<Vec<Chunk>>
}

impl TileMapLayer {
    fn new() -> TileMapLayer {
        let mut chunks = Vec::with_capacity(16);
        for x in 0..16 {
            let mut row = Vec::with_capacity(16);
            for y in 0..16 {
                row[y] = Chunk::new();
            }
            chunks[x] = row;
        }
        TileMapLayer { chunks }
    }
}

impl Chunk {
    pub fn new() -> Chunk {
        let mut tiles: [[Tile; 16]; 16] = Default::default();
        for x in 0..16 {
            for y in 0..16 {
                tiles[x][y] = Tile::new(0);
            }
        }
        Chunk { tiles }
    }
    
    pub fn get_tile(chunk: &Chunk, x: u8, y: u8) -> &Tile {
        if x >= 16 || y >= 16 {
            panic!("tile index {}, {} out of range", x, y);
        }
        &chunk.tiles[x as usize][y as usize]
    }
    
    pub fn set_tile(chunk: &mut Chunk, x: u8, y: u8, tile: Tile) {
        if !x >= 16 && !y >= 16 {
            chunk.tiles[x as usize][y as usize] = tile;
        }
    }
}

impl Tile {
    fn new(tile_id: u8) -> Tile {
        Tile { tile_id }
    }
}

impl Default for Tile {
    fn default() -> Self {
        Tile::new(0)
    }
}