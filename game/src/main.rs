use bevy::prelude::*;
use bevy_inspector_egui::quick;


fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0., 0., 0.)))
        .add_systems(Startup, setup)
        .add_plugins((
            DefaultPlugins,
            quick::WorldInspectorPlugin::default(),
        ))
        .run();
}

fn setup() {
    println!("super cool game, eh?")
}
